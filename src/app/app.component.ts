import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Input } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  formNombre: FormGroup;
  nombre = 0;
  

  ngOnInit(): void {
    this.formNombre = new FormGroup(
      {
        nombre: new FormControl(),
      }
    )
  
  }
  
  submittedNumber(): void {
    if(this.formNombre.value.nombre === null){
      this.formNombre.value.nombre = 0;
    } 
    this.nombre = this.formNombre.value.nombre;    
  }

}
